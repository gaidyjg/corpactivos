<style>
    .marquee-banner {
        height: 50px;
        color: #002fff;
        background-color: #f1f4f6;
        margin: 1rem 0;
        font-weight: 800;
        font-size: 25px;
        font-family: 'Open Sans', 'sans-serif';
        padding: 7px 0;
    }
</style>

<marquee class="marquee-banner">Precio de la Tasa de Cambio Hoy:
    Dólar - Bolívar: 260.289,19
    | Pesos - Bolívar: 52.631,58
</marquee>